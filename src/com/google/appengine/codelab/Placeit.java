package com.google.appengine.codelab;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;

import java.util.List;


public class Placeit 
{
	 /**
	   * Update the product
	   * @param name: name of the product
	   * @param description : description
	   * @return  updated product
	   */
	  public static void createOrUpdatePlaceit(String name, String description, String lat, String longi) {
	    Entity placeit = getSinglePlaceit(name);
	  	if (placeit == null) {
	  		placeit = new Entity("PlaceIt", name);
	  		placeit.setProperty("description", description);
	  		placeit.setProperty("lat", lat);
	  		placeit.setProperty("long", longi);
	  	} else {
	  		placeit.setProperty("description", description);
	  	}
	  	Util.persistEntity(placeit);
	  }

	  /**
	   * Retrun all the products
	   * @param kind : of kind product
	   * @return  products
	   */
	  public static Iterable<Entity> getAllPlaceit() {
		  	Iterable<Entity> entities = Util.listEntities("PlaceIt", null, null);
		  	return entities;
	  }

	  /**
	   * Get product entity
	   * @param name : name of the product
	   * @return: product entity
	   */
	  public static Entity getSinglePlaceit(String name) {
	  	Key key = KeyFactory.createKey("PlaceIt",name);
	  	return Util.findEntity(key);
	  }

	  /**
	   * Get all items for a product
	   * @param name: name of the product
	   * @return list of items
	   */
	  
	  public static List<Entity> getPlaceit(String name) {
	  	Query query = new Query();
	  	Key parentKey = KeyFactory.createKey("PlaceIt", name);
	  	query.setAncestor(parentKey);
	  	query.addFilter(Entity.KEY_RESERVED_PROPERTY, Query.FilterOperator.GREATER_THAN, parentKey);
	  		List<Entity> results = Util.getDatastoreServiceInstance()
	  				.prepare(query).asList(FetchOptions.Builder.withDefaults());
	  		return results;
		}
	  
	  /**
	   * Delete product entity
	   * @param productKey: product to be deleted
	   * @return status string
	   */
	  public static String deletePlaceit(String placeitKey)
	  {
		  Key key = KeyFactory.createKey("PlaceIt",placeitKey);	   
		  
		  List<Entity> placeits = getPlaceit(placeitKey);	  
		  if (!placeits.isEmpty()){
		      return "Cannot delete, as there are items associated with this product.";	      
		    }	    
		  Util.deleteEntity(key);
		  return "Product deleted successfully";
		  
	  }

}
